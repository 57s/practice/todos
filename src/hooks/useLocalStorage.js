import React from 'react';

function useLocalStorage(key, initialValue) {
	const getValue = () => {
		const localStorageValue = localStorage.getItem(key);

		if (localStorageValue) return JSON.parse(localStorageValue);
		return initialValue;
	};

	const [value, setValue] = React.useState(getValue);

	React.useEffect(() => {
		localStorage.setItem(key, JSON.stringify(value));
	}, [value, key]);

	return [value, setValue];
}

export default useLocalStorage;
