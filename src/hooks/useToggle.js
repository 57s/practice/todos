import React from 'react';

function useToggle(initialValue = false) {
	const [isOpen, setOpen] = React.useState(initialValue);

	const toggleOpen = function () {
		setOpen(!isOpen);
	};

	return [isOpen, toggleOpen];
}

export default useToggle;
