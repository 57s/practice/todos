import React from 'react';

function useInput(defaultValue = '', onChangeHandler) {
	const [value, setValue] = React.useState(defaultValue);

	const onChange = event => {
		if (onChangeHandler) onChangeHandler(event.target.value);
		setValue(event.target.value);
	};

	const changeValue = newValue => {
		setValue(newValue);
	};

	return {
		value,
		onChange,
		setValue: changeValue,
	};
}

export { useInput };
