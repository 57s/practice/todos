import React from 'react';

function useOutsideClick(elementRef, handler, attached) {
	React.useEffect(() => {
		if (!attached) return;

		const handleClick = function (event) {
			if (!elementRef.current) return;
			if (!elementRef.current.contains(event.target)) handler();
		};

		document.addEventListener('click', handleClick);

		return () => {
			document.removeEventListener('click', handleClick);
		};
	}, [elementRef, handler, attached]);
}

export default useOutsideClick;
