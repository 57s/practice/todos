import React from 'react';
import { createRoot } from 'react-dom/client';

import './styles/index.css';
import App from './components/templates/app/App';

createRoot(document.getElementById('root')).render(<App />);
