import React from 'react';

import Button from '../../atoms/button/Button';
import './ControlTab.css';

function ControlTab({ title, value, isActive, onClick }) {
	return (
		<div
			className={isActive ? 'control-tab control-tab_active' : 'control-tab'}>
			<Button onClick={onClick} className="control-tab__button">
				{title}
			</Button>
			<div className="control-tab__bubble">{value}</div>
		</div>
	);
}
export default ControlTab;
