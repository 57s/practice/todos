import React from 'react';

import Accordion from '../../atoms/accordion/Accordion';
import Switch from '../../atoms/switch/Switch';
import ControlTodos from '../controlTodos/ControlTodos';
import SearchTodos from '../searchTodos/SearchTodos';
import './ControlPanelTodos.css';

function ControlPanelTodos({ isSearchMod, changeSearchMod }) {
	return (
		<div className="todos-control">
			<Accordion title={'control'}>
				<div className="todos-control__inner">
					<div>
						<Switch
							ofContent="of"
							onContent="on"
							onChange={changeSearchMod}
							checked={isSearchMod}
						/>
					</div>

					{!isSearchMod && <ControlTodos />}
					{isSearchMod && <SearchTodos />}
				</div>
			</Accordion>
		</div>
	);
}

export default ControlPanelTodos;
