import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBan } from '@fortawesome/free-solid-svg-icons';

import { SHOW_MOD } from '../../../store/todos/todoBoilerplate';
import { TodosContext } from '../../../store/todos/TodosProvider';

import Select from '../../atoms/select/Select';
import Button from '../../atoms/button/Button';
import ControlTab from '../controlTab/ControlTab';

import './ControlTodos.css';

function ControlTodos() {
	const {
		showMode,
		quantityAllTodos,
		quantityCompletedTodos,
		quantityActiveTodos,
		onDeleteCompleted,
		onShowModTodo,
		onSortTodo,
	} = React.useContext(TodosContext);

	const options = [
		{ title: 'Time', value: 'time' },
		{ title: 'New', value: 'new' },
		{ title: 'Active', value: 'active' },
		{ title: 'Done', value: 'done' },
	];

	return (
		<div className="control-todos">
			<div className="control-todos__left">
				<div>Sort:</div>
				<Select options={options} onChange={onSortTodo} />
			</div>

			<div className="control-todos__center">
				<div>Show: </div>

				<div className="control-todos__tab-box">
					<ControlTab
						title="All"
						value={quantityAllTodos}
						isActive={SHOW_MOD.all === showMode}
						onClick={() => onShowModTodo(SHOW_MOD.all)}
					/>

					<ControlTab
						title="Active"
						value={quantityActiveTodos}
						isActive={SHOW_MOD.active === showMode}
						onClick={() => onShowModTodo(SHOW_MOD.active)}
					/>

					<ControlTab
						title="Done"
						value={quantityCompletedTodos}
						isActive={SHOW_MOD.done === showMode}
						onClick={() => onShowModTodo(SHOW_MOD.done)}
					/>
				</div>
			</div>

			<div className="control-todos__right">
				<Button
					onClick={onDeleteCompleted}
					disabled={quantityCompletedTodos <= 0}
					title={
						quantityCompletedTodos <= 0
							? 'Нет выполненных'
							: 'Удалить выполненные'
					}>
					<FontAwesomeIcon icon={faBan} className="icon" />
				</Button>
			</div>
		</div>
	);
}
export default ControlTodos;
