import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import Input from '../../atoms/input/Input';
import useBlockTodosNew from './useBlockTodosNew';

import './BlockTodosNew.css';

function BlockTodosNew() {
	const { newTodoInput, isDisable, handler, keyHandler, setValue } =
		useBlockTodosNew();

	return (
		<div className="block-todos-new">
			<Input
				onKeyDown={keyHandler}
				{...newTodoInput}
				onClear={setValue}
				className="block-todos-new_input"
				type="text"
				placeholder="Add todo"
			/>

			<button
				className="block-todos-new__button button"
				disabled={isDisable}
				onClick={handler}
				title={isDisable ? 'Не менее трех символов' : 'Добавить задачу'}>
				<FontAwesomeIcon
					icon={faPlus}
					className="block-todos-new__button-icon"
				/>
			</button>
		</div>
	);
}
export default BlockTodosNew;
