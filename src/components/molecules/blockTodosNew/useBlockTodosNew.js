import React from 'react';
import { TodosContext } from '../../../store/todos/TodosProvider';
import { useInput } from '../../../hooks/useInput';

function useBlockTodosNew() {
	const [isDisable, setDisable] = React.useState(true);
	const { onAdd } = React.useContext(TodosContext);
	const { setValue, ...newTodoInput } = useInput();
	const lengthValue = newTodoInput.value.length;

	React.useEffect(() => {
		if (lengthValue < 3) {
			setDisable(true);
		} else {
			setDisable(false);
		}
	}, [lengthValue]);

	const handler = () => {
		if (lengthValue >= 3) {
			onAdd(newTodoInput.value);
			setValue('');
		} else {
			alert('Поле не должно быть пустым!!!');
		}
	};

	const keyHandler = event => {
		if (event.key === 'Enter') handler();
		if (event.key === 'Delete') setValue('');
	};

	return {
		newTodoInput,
		isDisable,
		handler,
		keyHandler,
		setValue,
	};
}

export default useBlockTodosNew;
