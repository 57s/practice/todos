import React from 'react';

import useBlockTodos from './useBlockTodos';

import ControlPanelTodos from '../controlPanelTodos/ControlPanelTodos';
import ListTodos from '../listTodos/ListTodos';
import EmptyTodos from '../../atoms/emptyTodos/EmptyTodos';

import './BlockTodos.css';

function TodosBlock() {
	const { todos, quantityAllTodos, isSearchMod, changeSearchMod } =
		useBlockTodos();

	return (
		<div className="block-todos">
			{quantityAllTodos > 0 && (
				<ControlPanelTodos
					isSearchMod={isSearchMod}
					changeSearchMod={changeSearchMod}
				/>
			)}

			{!isSearchMod && todos.length <= 0 && (
				<EmptyTodos
					message={
						<>
							<p>У вас еще нет зарегистрированных задач.</p>
							<p>Создавайте задачи и организуйте свои дела!</p>
						</>
					}
				/>
			)}

			{!isSearchMod && todos.length > 0 && (
				<ListTodos arrTodos={todos} isSearchMod={isSearchMod} />
			)}

			{isSearchMod && todos.length > 0 && (
				<ListTodos arrTodos={todos} isSearchMod={isSearchMod} />
			)}

			{isSearchMod && todos.length <= 0 && (
				<EmptyTodos message={<p>Не чего не найдено</p>} />
			)}
		</div>
	);
}
export default TodosBlock;
