import React from 'react';

import useToggle from '../../../hooks/useToggle';
import { TodosContext } from '../../../store/todos/TodosProvider';

function useBlockTodos() {
	const { todos, quantityAllTodos } = React.useContext(TodosContext);
	const [isOpen, toggleOpen] = useToggle(false);

	return {
		todos,
		quantityAllTodos,
		isSearchMod: isOpen,
		changeSearchMod: toggleOpen,
	};
}

export default useBlockTodos;
