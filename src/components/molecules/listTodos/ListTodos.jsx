import React from 'react';

import ItemTodo from '../itemTodo/ItemTodo';
import './ListTodos.css';

function ListTodos({ arrTodos }) {
	return (
		<div className="list-todos">
			{arrTodos.map((item, index) => (
				<div className="list-todos__item" key={item.id}>
					<div className="list-todos__item-index">{index + 1}</div>
					<ItemTodo {...item} />
				</div>
			))}
		</div>
	);
}
export default ListTodos;
