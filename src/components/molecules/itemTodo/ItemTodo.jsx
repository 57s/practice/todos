import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faPen, faTrash } from '@fortawesome/free-solid-svg-icons';

import useTodo from './useTodos';
import CheckBox from '../../atoms/checkBox/CheckBox';
import Input from '../../atoms/input/Input';
import Button from '../../atoms/button/Button';
import './ItemTodo.css';

function ItemTodo(props) {
	const {
		title,
		todoEditInput,
		completed,
		isChecked,
		isEditMod,
		inputRef,
		onToggleEditMod,
		onCompletedTodo,
		onRemoveTodo,
		onEditTodo,
		setValue,
		keyHandler,
	} = useTodo(props);

	return (
		<div className={completed ? 'item-todo item-todo_completed' : 'item-todo'}>
			<label className="item-todo__label">
				<CheckBox
					checked={isChecked}
					onChange={isEditMod ? () => {} : onCompletedTodo}
				/>

				{!isEditMod && <div className="item-todo__title">{title}</div>}

				{isEditMod && (
					<Input
						onKeyDown={keyHandler}
						{...todoEditInput}
						onClear={setValue}
						inputRef={inputRef}
					/>
				)}
			</label>

			<div className="item-todo__control">
				{!completed && (
					<Button
						onClick={isEditMod ? onEditTodo : onToggleEditMod}
						title={isEditMod ? 'Сохранить' : 'Изменить'}>
						{isEditMod ? (
							<FontAwesomeIcon icon={faCheck} className="icon" />
						) : (
							<FontAwesomeIcon icon={faPen} className="icon" />
						)}
					</Button>
				)}

				<Button onClick={onRemoveTodo} title="Удалить" disabled={isEditMod}>
					<FontAwesomeIcon icon={faTrash} className="icon" />
				</Button>
			</div>
		</div>
	);
}
export default ItemTodo;
