import React from 'react';

import { TodosContext } from '../../../store/todos/TodosProvider';
import { useInput } from '../../../hooks/useInput';
import useToggle from '../../../hooks/useToggle';

function useTodo({ id, title, completed: isCompleted }) {
	const { setValue, ...todoEditInput } = useInput(title);
	const inputRef = React.useRef(null);
	const { onCompleted, onDelete, onEdit } = React.useContext(TodosContext);
	const [completed, setCompleted] = React.useState(isCompleted);
	const [isEditMod, toggleMod] = useToggle();
	const [isChecked, setChecked] = React.useState(isCompleted);

	React.useEffect(() => {
		// if (isEditMod) inputRef?.current?.focus();
		if (isEditMod) inputRef?.current?.select();
	}, [isEditMod]);

	const onRemoveTodo = event => {
		event.stopPropagation();
		onDelete(id);
	};

	const onCompletedTodo = () => {
		setCompleted(!completed);
		onCompleted(id);
		setChecked(!isChecked);
	};

	const onToggleEditMod = event => {
		event.stopPropagation();
		toggleMod();
	};

	const onEditTodo = event => {
		event.stopPropagation();
		onEdit(id, todoEditInput.value);
		onToggleEditMod(event);
	};

	const keyHandler = event => {
		if (event.key === 'Enter') {
			onEdit(id, todoEditInput.value);
			onToggleEditMod(event);
		}

		if (event.key === 'Delete') setValue('');
	};

	return {
		title,
		todoEditInput,
		completed,
		isChecked,
		isEditMod,
		inputRef,
		onToggleEditMod,
		onRemoveTodo,
		onCompletedTodo,
		onEditTodo,
		setValue,
		keyHandler,
	};
}

export default useTodo;
