import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';

import useSearchTodos from './useSearchTodos';
import Input from '../../atoms/input/Input';
import Button from '../../atoms/button/Button';
import './SearchTodos.css';

function SearchTodos() {
	const { inputSearch, onClickSearch, onClearSearchValue } = useSearchTodos();

	return (
		<div className="search-todos">
			<div className="search-todos__title">Search: </div>

			<Input
				{...inputSearch}
				onClear={onClearSearchValue}
				placeholder="Find todo"
			/>

			<div className="search-todos__control">
				<Button onClick={onClickSearch} title="Найти">
					<FontAwesomeIcon icon={faMagnifyingGlass} className="icon" />
				</Button>
			</div>
		</div>
	);
}
export default SearchTodos;
