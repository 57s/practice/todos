import React from 'react';

import { TodosContext } from '../../../store/todos/TodosProvider';
import { useInput } from '../../../hooks/useInput';
function useSearchTodos() {
	const { onSearchTodo } = React.useContext(TodosContext);
	const { setValue, ...inputSearch } = useInput('', onSearchTodo);

	React.useEffect(() => {
		return () => {
			onSearchTodo('');
		};
	}, [onSearchTodo]);

	const onClickSearch = () => {
		onSearchTodo(inputSearch.value);
	};

	const onClearSearchValue = () => {
		onSearchTodo('');
		setValue('');
	};

	return {
		inputSearch,
		setValue,
		onClickSearch,
		onClearSearchValue,
	};
}

export default useSearchTodos;
