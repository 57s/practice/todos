import React from 'react';

import { AppContext } from '../../../store/app/AppProvider';

import Container from '../container/Container';
import Header from '../../organisms/header/Header';

import './Layout.css';

function Layout(children) {
	const { themeDark } = React.useContext(AppContext);

	return (
		<div
			className={
				themeDark ? 'page theme theme_dark' : 'page theme theme_light'
			}>
			<Header className="page__header" />
			<main className="page__main">
				<Container>{children.children}</Container>
			</main>
		</div>
	);
}
export default Layout;
