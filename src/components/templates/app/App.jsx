import React from 'react';

import { AppProvider } from '../../../store/app/AppProvider';
import PageTodos from '../../page/todos/PageTodos';

function App() {
	return (
		<AppProvider>
			<PageTodos />
		</AppProvider>
	);
}
export default App;
