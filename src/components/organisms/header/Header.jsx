import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faCalendarCheck,
	faMoon,
	faSun,
} from '@fortawesome/free-solid-svg-icons';

import { AppContext } from '../../../store/app/AppProvider';

import Switch from '../../atoms/switch/Switch';
import Container from '../../templates/container/Container';

import './Header.css';

function Header({ className, ...restProps }) {
	const { themeDark, onToggleTheme } = React.useContext(AppContext);

	return (
		<div
			className={className ? `${className} header` : 'header'}
			{...restProps}>
			<Container>
				<div className="header__inner">
					<div className="header__logo">
						<a href="/">
							<FontAwesomeIcon
								icon={faCalendarCheck}
								className="header__logo-icon"
							/>
						</a>
					</div>

					<h2 className="header__title">Todos</h2>

					<div className="header__theme">
						<div className="header__theme-text">Theme: </div>
						<Switch
							checked={themeDark}
							onChange={onToggleTheme}
							ofContent={<FontAwesomeIcon icon={faSun} />}
							onContent={<FontAwesomeIcon icon={faMoon} />}
						/>
					</div>
				</div>
			</Container>
		</div>
	);
}
export default Header;
