import React from 'react';

import { TodosProvider } from '../../../store/todos/TodosProvider';
import Box from '../../atoms/box/Box';
import TodosBlock from '../../molecules/blockTodos/BlockTodos';
import BlockTodosNew from '../../molecules/blockTodosNew/BlockTodosNew';
import Layout from '../../templates/Layout/Layout';

import './PageTodos.css';
function PageTodos() {
	return (
		<TodosProvider>
			<Layout>
				<section className="todos-page__section">
					<div className="todos-page__box-todos-new">
						<Box>
							<BlockTodosNew />
						</Box>
					</div>

					<Box>
						<TodosBlock />
					</Box>
				</section>
			</Layout>
		</TodosProvider>
	);
}
export default PageTodos;
