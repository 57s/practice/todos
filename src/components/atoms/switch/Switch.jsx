import React from 'react';

import './Switch.css';

function Switch({ ofContent, onContent, ...restProps }) {
	return (
		<div className={restProps.checked ? 'switch switch_on' : 'switch'}>
			<label className="switch__label">
				<input
					className="switch__input hidden "
					type="checkbox"
					{...restProps}
				/>

				<div className="switch__carriage"></div>
				<div className="switch__of">{ofContent}</div>
				<div className="switch__on">{onContent}</div>
			</label>
		</div>
	);
}
export default Switch;
