import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

import './CheckBox.css';

function CheckBox(props) {
	return (
		<div className="check-box">
			<input
				className="check-box__default hidden "
				type="checkbox"
				{...props}
			/>

			<div className="check-box__custom">
				<FontAwesomeIcon icon={faCheck} className="check-box__icon" />
			</div>
		</div>
	);
}
export default CheckBox;
