import React from 'react';

import './Button.css';

function Button({ children, className, isActive = false, ...restProps }) {
	className = className ?? '';
	return (
		<button
			className={
				isActive ? `${className} button button_active` : `${className} button`
			}
			{...restProps}>
			<div className="button__inner">{children}</div>
		</button>
	);
}
export default Button;
