import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';

import useToggle from '../../../hooks/useToggle';
import './Accordion.css';

function Accordion({ title, isOpen: startIsOpen = false, children }) {
	const [isOpen, toggleOpen] = useToggle(startIsOpen);

	return (
		<div
			className={isOpen ? 'accordion accordion_open' : 'accordion'}
			title={isOpen ? 'Скрыть' : 'Показать'}>
			<div className="accordion__header" onClick={toggleOpen}>
				<h3 className="accordion__title">{title}</h3>
				<FontAwesomeIcon
					icon={faCaretDown}
					className="accordion__header-icon"
				/>
			</div>

			<div className="accordion__body">{children}</div>
		</div>
	);
}
export default Accordion;
