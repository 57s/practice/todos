import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';

import useSelect from './useSelect';
import './Select.css';

function Select(props) {
	const { isOpen, selectRef, selected, options, changeHandler, toggleOpen } =
		useSelect(props);

	return (
		<div className={isOpen ? 'select select__open' : 'select'} ref={selectRef}>
			<header className="select__header" onClick={toggleOpen}>
				<h3 className="select__selected" data-value={selected.value}>
					{selected.title}
				</h3>
				<FontAwesomeIcon icon={faCaretDown} className="select__header-icon" />
			</header>

			<div className="select__body">
				{options.map(option => (
					<div
						className="select__item"
						data-value={option.value}
						onClick={changeHandler}
						key={option.value}>
						{option.title}
					</div>
				))}
			</div>
		</div>
	);
}

export default Select;
