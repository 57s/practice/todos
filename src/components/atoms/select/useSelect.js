import React from 'react';

import useToggle from '../../../hooks/useToggle';
import useOutsideClick from '../../../hooks/useOutsideClick';

function useSelect({ options, onChange }) {
	const selectRef = React.useRef(null);
	const [isOpen, toggleOpen] = useToggle();
	const [selected, setSelected] = React.useState({
		title: 'Выбрать',
		value: 0,
	});

	const changeHandler = function (event) {
		const currentOption = {
			title: event.target.textContent,
			value: event.target.dataset.value,
		};

		setSelected(currentOption);
		onChange(event.target.dataset.value);
		toggleOpen();
	};

	useOutsideClick(selectRef, toggleOpen, isOpen);

	React.useEffect(() => {
		return () => {
			onChange('new');
		};
	}, [onChange]);

	return { isOpen, selectRef, selected, options, changeHandler, toggleOpen };
}

export default useSelect;
