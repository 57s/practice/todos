import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

import './Input.css';

function Input({ className, onClear, inputRef, ...restProps }) {
	return (
		<div className="input-box">
			<input
				ref={inputRef}
				{...restProps}
				className={
					className ? `${className} input-box__input` : 'input-box__input'
				}
				type="text"
			/>

			{onClear && restProps.value.length > 0 && (
				<button
					className="input-box__button-clear button"
					onClick={() => onClear('')}
					title="Очистить">
					<FontAwesomeIcon
						icon={faXmark}
						className="input-box__button--icon icon"
					/>
				</button>
			)}
		</div>
	);
}
export default Input;
