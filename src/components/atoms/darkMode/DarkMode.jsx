import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon, faSun } from '@fortawesome/free-solid-svg-icons';

import './DarkMode.css';

const DarkMode = ({ isDark, onToggleTheme }) => {
	return (
		<div className="theme-toggle">
			<label className="theme-toggle__label">
				<input
					className="theme-toggle__input hidden"
					type="checkbox"
					checked={isDark}
					onChange={onToggleTheme}
				/>

				<div className="theme-toggle__carriage"></div>

				<FontAwesomeIcon
					icon={faSun}
					className={
						isDark
							? 'theme-toggle__icon theme-toggle__icon_son theme-toggle__icon_active '
							: 'theme-toggle__icon theme-toggle__icon_son'
					}
				/>
				<FontAwesomeIcon
					icon={faMoon}
					className={
						isDark
							? 'theme-toggle__icon theme-toggle__icon_moon theme-toggle__icon_active '
							: 'theme-toggle__icon theme-toggle__icon_moon'
					}
				/>
			</label>
		</div>
	);
};

export default DarkMode;
