import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHourglass } from '@fortawesome/free-solid-svg-icons';

import './EmptyTodos.css';

function EmptyTodos({ message }) {
	return (
		<div className="empty-todos">
			<FontAwesomeIcon icon={faHourglass} className="empty-todos__icon" />

			<div className="empty-todos__description">{message}</div>
		</div>
	);
}
export default EmptyTodos;
