function sortObject(arr, property, reverse = false) {
	return [
		...arr.sort((a, b) =>
			reverse ? a[property] - b[property] : b[property] - a[property]
		),
	];
}

export default sortObject;
