export const DELETE = 'delete';
export const DELETE_COMPLETED = 'delete-completed';
export const ADD = 'add';
export const COMPLETED = 'completed';
export const EDIT = 'edit';
export const SORT = 'sort';
export const SEARCH = 'search';
export const SHOW = 'show';

export const SORT_TYPES = {
	new: 'new',
	time: 'time',
	done: 'done',
	active: 'active',
};

export const SHOW_MOD = {
	all: 'all',
	done: 'done',
	active: 'active',
};
