import {
	DELETE,
	DELETE_COMPLETED,
	ADD,
	COMPLETED,
	EDIT,
	SORT,
	SEARCH,
	SHOW,
} from './todoBoilerplate';

const removeTodo = id => ({ type: DELETE, payload: id });
const removeCompletedTodo = () => ({ type: DELETE_COMPLETED });
const addTodo = title => ({ type: ADD, payload: title });
const toggleCompletedTodo = id => ({ type: COMPLETED, payload: id });
const editTodo = payload => ({ type: EDIT, payload: { ...payload } });
const sortTodo = sortType => ({ type: SORT, payload: sortType });
const searchTodo = searchValue => ({ type: SEARCH, payload: searchValue });
const showTodo = showMod => ({ type: SHOW, payload: showMod });

export {
	removeTodo,
	addTodo,
	toggleCompletedTodo,
	editTodo,
	removeCompletedTodo,
	sortTodo,
	searchTodo,
	showTodo,
};
