import React from 'react';

import {
	removeTodo,
	removeCompletedTodo,
	addTodo,
	toggleCompletedTodo,
	editTodo,
	sortTodo,
	searchTodo,
	showTodo,
} from './todoActionsGenerator';
import { SORT_TYPES, SHOW_MOD } from './todoBoilerplate';
import sortObject from '../../utils/sort';
import useLocalStorage from '../../hooks/useLocalStorage';
import reducer from './todoReducer';

const startState = {
	todos: [],
	searchValue: '',
	sortType: SORT_TYPES.new,
	showMode: SHOW_MOD.all,
};

function useTodosProvider() {
	const [initState, setInitState] = useLocalStorage('stateTodo', startState);
	const [state, dispatch] = React.useReducer(reducer, initState);

	const completedTodos = state.todos.filter(todo => todo.completed);
	const activeTodos = state.todos.filter(todo => !todo.completed);

	const quantityAllTodos = state.todos.length;
	const quantityCompletedTodos = completedTodos.length;
	const quantityActiveTodos = activeTodos.length;

	const searchTodos = (arrTodos, searchValue) => {
		return arrTodos.filter(todo => todo.title.includes(searchValue));
	};

	let todos;

	const sortTodos = (arrTodo, typeSort) => {
		if (typeSort === SORT_TYPES.new)
			return sortObject(arrTodo, 'creationTime', false);

		if (typeSort === SORT_TYPES.time)
			return sortObject(arrTodo, 'number', true);

		if (typeSort === SORT_TYPES.done)
			return sortObject(arrTodo, 'completed', false);

		if (typeSort === SORT_TYPES.active)
			return sortObject(arrTodo, 'completed', true);
	};

	if (state.showMode === SHOW_MOD.active) {
		todos = sortTodos(activeTodos, state.sortType);
	} else if (state.showMode === SHOW_MOD.done) {
		todos = sortTodos(completedTodos, state.sortType);
	} else {
		todos = sortTodos(state.todos, state.sortType);
	}

	if (state.searchValue.length > 0) {
		todos = searchTodos(state.todos, state.searchValue);
	}

	React.useEffect(() => {
		setInitState(state);
	}, [setInitState, state]);

	const onDelete = React.useCallback(id => {
		dispatch(removeTodo(id));
	}, []);

	const onDeleteCompleted = React.useCallback(() => {
		dispatch(removeCompletedTodo());
	}, []);

	const onAdd = React.useCallback(todo => {
		dispatch(addTodo(todo));
	}, []);

	const onCompleted = React.useCallback(id => {
		dispatch(toggleCompletedTodo(id));
	}, []);

	const onEdit = React.useCallback((id, title) => {
		dispatch(editTodo({ id, title }));
	}, []);

	const onSortTodo = React.useCallback(sortType => {
		dispatch(sortTodo(sortType));
	}, []);

	const onSearchTodo = React.useCallback(searchValue => {
		dispatch(searchTodo(searchValue));
	}, []);

	const onShowModTodo = React.useCallback(showMode => {
		dispatch(showTodo(showMode));
	}, []);

	const todoState = {
		sortType: state.sortType,
		showMode: state.showMode,
		todos,
		quantityAllTodos,
		quantityCompletedTodos,
		quantityActiveTodos,
		onDelete,
		onDeleteCompleted,
		onAdd,
		onCompleted,
		onEdit,
		onSortTodo,
		onSearchTodo,
		onShowModTodo,
	};

	return { todoState };
}

export default useTodosProvider;
