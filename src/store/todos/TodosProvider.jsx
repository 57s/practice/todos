import React from 'react';

import useTodosProvider from './useTodosProvider';

const TodosContext = React.createContext();

const TodosProvider = ({ children }) => {
	const { todoState } = useTodosProvider();

	return (
		<TodosContext.Provider value={todoState}>{children}</TodosContext.Provider>
	);
};

export { TodosProvider, TodosContext };
