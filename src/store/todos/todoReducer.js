import {
	DELETE,
	DELETE_COMPLETED,
	ADD,
	COMPLETED,
	EDIT,
	SORT,
	SEARCH,
	SHOW,
} from './todoBoilerplate';

const todoReducer = (state, action) => {
	switch (action.type) {
		case DELETE: {
			const id = action.payload;

			return {
				...state,
				todos: state.todos.filter(todo => todo.id !== id),
			};
		}

		case DELETE_COMPLETED: {
			return {
				...state,
				todos: state.todos.filter(todo => todo.completed !== true),
			};
		}

		case ADD: {
			const title = action.payload;
			const arrNumbers = state.todos.map(todo => todo.number);
			const newNumber =
				arrNumbers.length === 0 ? 1 : Math.max(...arrNumbers) + 1;

			const newTodo = {
				id: window.crypto.randomUUID(),
				number: newNumber,
				title: title,
				completed: false,
				creationTime: new Date(),
				update: false,
			};

			return {
				...state,
				todos: [newTodo, ...state.todos],
			};
		}

		case COMPLETED: {
			const id = action.payload;

			return {
				...state,
				todos: state.todos.map(todo =>
					todo.id === id ? { ...todo, completed: !todo.completed } : todo
				),
			};
		}

		case EDIT: {
			const { id, title: newTitle } = action.payload;

			return {
				...state,
				todos: state.todos.map(todo =>
					todo.id === id
						? { ...todo, title: newTitle, update: new Date() }
						: todo
				),
			};
		}

		case SORT: {
			const sortType = action.payload;

			return {
				...state,
				sortType,
			};
		}

		case SEARCH: {
			const searchValue = action.payload;

			return {
				...state,
				searchValue,
			};
		}

		case SHOW: {
			const showMode = action.payload;

			return {
				...state,
				showMode,
			};
		}

		default:
			return { ...state };
	}
};

export default todoReducer;
