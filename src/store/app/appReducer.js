import { TOGGLE_THEME } from './appBoilerplate';

function appReducer(state, action) {
	switch (action.type) {
		case TOGGLE_THEME: {
			return { ...state, themeDark: !state.themeDark };
		}

		default: {
			return {
				...state,
			};
		}
	}
}

export default appReducer;
