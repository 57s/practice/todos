import { TOGGLE_THEME } from './appBoilerplate';

const switchTheme = () => ({ type: TOGGLE_THEME });

export { switchTheme };
