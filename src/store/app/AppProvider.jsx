import React from 'react';
import { useAppProvider } from './useAppProvider';
const AppContext = React.createContext();

function AppProvider({ children }) {
	const appState = useAppProvider();

	return <AppContext.Provider value={appState}>{children}</AppContext.Provider>;
}
export { AppProvider, AppContext };
