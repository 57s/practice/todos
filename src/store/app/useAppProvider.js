import React from 'react';

import useLocalStorage from '../../hooks/useLocalStorage';
import { switchTheme } from './appActionsGenerator';
import appReducer from './appReducer';

function useAppProvider() {
	const startState = { themeDark: false };
	const [initState, setInitState] = useLocalStorage('stateApp', startState);
	const [state, dispatch] = React.useReducer(appReducer, initState);

	const onToggleTheme = React.useCallback(() => {
		dispatch(switchTheme());
	}, []);

	React.useEffect(() => {
		setInitState(state);
	}, [state, setInitState]);

	const appState = { ...state, onToggleTheme };

	return appState;
}

export { useAppProvider };
